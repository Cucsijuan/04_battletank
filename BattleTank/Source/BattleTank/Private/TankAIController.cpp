// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "Tank.h"

void ATankAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	auto PlayerPawn = Cast<ATank>(GetWorld()->GetFirstPlayerController()->GetPawn());
	auto ControlledTank = Cast<ATank>(GetPawn());
	if (PlayerPawn)
	{
		//TODO Move towards the player

		// Aim Towards the player
		ControlledTank->AimAt(PlayerPawn->GetActorLocation());

		// fire if ready
		ControlledTank->Fire();//TODO dont Fire every frame
	}
}
